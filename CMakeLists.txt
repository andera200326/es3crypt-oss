cmake_minimum_required(VERSION 3.5)

project(es3crypt LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_library(es3crypt SHARED es3crypt.cpp es3crypt.hpp)
target_link_libraries(es3crypt PRIVATE cryptopp)

option(ES3CRYPT_BUILD_CLI "Build the CLI frontend" ON)
if (ES3CRYPT_BUILD_CLI)
    add_executable(es3crypt-cli cli.cpp)
    target_link_libraries(es3crypt-cli PUBLIC es3crypt)
endif()

option(ES3CRYPT_BUILD_WEB "Build the web frontend" OFF)
if (ES3CRYPT_BUILD_WEB)
    option(ES3CRYPT_WEB_LAYOUT "Set save file layout file to use in web frontend" "none")
    add_executable(es3crypt-web web.cpp)
    target_link_libraries(es3crypt-web PUBLIC es3crypt pthread)
    if (ES3CRYPT_WEB_LAYOUT STREQUAL "none")
        message(WARNING "Not using any layout file for web frontend, allowing upload of arbitrary data instead.")
    else()
        message(STATUS "Using ${ES3CRYPT_WEB_LAYOUT} as layout file for web frontend.")
        target_compile_definitions(es3crypt-web PRIVATE ES3CRYPT_WEB_LAYOUT="${ES3CRYPT_WEB_LAYOUT}")
    endif()
endif()
