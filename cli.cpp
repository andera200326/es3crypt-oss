/*
    es3crypt-oss
    Copyright (C) 2022  niansa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "es3crypt.hpp"

#include <iostream>
#include <fstream>
#include <string_view>



int main(int argc, char **argv) {
    // Check args
    if (argc < 6) {
        std::cout << "Usage: " << argv[0] << " decrypt/encrypt <password> <buffer size> <input> <output>" << std::endl;
        return 1;
    }
    // Get args
    std::string_view mode = argv[1],
                     password = argv[2];
    unsigned bufSize = std::atoi(argv[3]);
    auto in_file_path = argv[4],
         out_file_path = argv[5];
    // Open input/output file
    std::fstream in_file(in_file_path, std::ios::openmode::_S_in | std::ios::openmode::_S_bin);
    std::fstream out_file(out_file_path, std::ios::openmode::_S_out | std::ios::openmode::_S_bin);
    // Encrypt or decrypt
    if (mode == "decrypt") {
        while (in_file) {
            ES3Crypt::decrypt(in_file, out_file, password, bufSize);
        }
    } else if (mode == "encrypt") {
        while (in_file) {
            ES3Crypt::encrypt(in_file, out_file, password, bufSize);
        }
    }
}
